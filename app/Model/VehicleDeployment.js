'use strict'

const Lucid = use('Lucid')

class VehicleDeployment extends Lucid {
	static get table () {
    	return 'vehicle_deployment'
  	}
}

module.exports = VehicleDeployment
