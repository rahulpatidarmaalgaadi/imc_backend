'use strict'

const Lucid = use('Lucid')

class CommercialAreas extends Lucid {
	static get table () {
    	return 'commercial_areas'
  	}
}

module.exports = CommercialAreas
