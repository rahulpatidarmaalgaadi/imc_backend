'use strict'

const Lucid = use('Lucid')

class Rwas extends Lucid {
	static get table () {
    	return 'rwas'
  	}
}

module.exports = Rwas
