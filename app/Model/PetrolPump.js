'use strict'

const Lucid = use('Lucid')

class PetrolPump extends Lucid {
	
	static get table () {
	    return 'petrol_pump'
    }
}

module.exports = PetrolPump

