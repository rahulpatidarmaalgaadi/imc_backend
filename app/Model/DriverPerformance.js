'use strict'

const Lucid = use('Lucid')

class DriverPerformance extends Lucid {
	static get table () {
    	return 'driver_performance'
  	}
}

module.exports = DriverPerformance
