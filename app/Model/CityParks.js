'use strict'

const Lucid = use('Lucid')

class CityParks extends Lucid {
	static get table () {
    	return 'city_parks'
  	}
}

module.exports = CityParks
