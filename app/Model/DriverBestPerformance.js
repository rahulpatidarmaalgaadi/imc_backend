'use strict'

const Lucid = use('Lucid')

class DriverBestPerformance extends Lucid {
	static get table () {
    	return 'driver_best_performance'
  	}
}

module.exports = DriverBestPerformance
