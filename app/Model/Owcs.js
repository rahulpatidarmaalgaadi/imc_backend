'use strict'

const Lucid = use('Lucid')

class Owcs extends Lucid {
	static get table () {
    	return 'owcs'
  	}
}

module.exports = Owcs
