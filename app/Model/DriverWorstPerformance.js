'use strict'

const Lucid = use('Lucid')

class DriverWorstPerformance extends Lucid {
	static get table () {
    	return 'driver_worst_performance'
  	}
}

module.exports = DriverWorstPerformance
