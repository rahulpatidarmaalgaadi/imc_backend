'use strict'

const Lucid = use('Lucid')

class Gvps extends Lucid {
	static get table () {
    	return 'gvps'
  	}
}

module.exports = Gvps
