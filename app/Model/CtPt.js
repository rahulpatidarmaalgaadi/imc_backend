'use strict'

const Lucid = use('Lucid')

class CtPt extends Lucid {
	static get table () {
    	return 'ct_pt'
  	}
}

module.exports = CtPt
