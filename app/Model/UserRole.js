'use strict'

const Lucid = use('Lucid')

class UserRole extends Lucid {
  static get table () {
      return 'user_role'
    }
}

module.exports = UserRole