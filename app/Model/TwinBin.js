'use strict'

const Lucid = use('Lucid')

class TwinBin extends Lucid {
	static get table () {
    	return 'twin_bins'
  	}
}

module.exports = TwinBin
