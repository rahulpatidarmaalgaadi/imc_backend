'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.any('/').render('welcome')
Route.any('/getPetrolPumpList', 'GisController.getPetrolPumpList')
Route.any('/getTwinBinsList', 'GisController.getTwinBinsList')
Route.any('/getCTPTList', 'GisController.getCTPTList')
Route.any('/getGvpsList', 'GisController.getGvpsList')
Route.any('/getCommercialAreasList', 'GisController.getCommercialAreasList')
Route.any('/getCityParksList', 'GisController.getCityParksList')
Route.any('/getRwasList', 'GisController.getRwasList')
Route.any('/getOwcsList', 'GisController.getOwcsList')

Route.any('/upload').render('upload')
Route.any('/uploadFile', 'PostsController.uploadFile')

// Geo Json
Route.any('/storePump', 'PostsController.storePump')
Route.any('/storeCtpt', 'PostsController.storeCtpt')
Route.any('/storeGvps', 'PostsController.storeGvps')
Route.any('/storeTwinBin', 'PostsController.storeTwinBin')
Route.any('/storeCityParks', 'PostsController.storeCityParks')
Route.any('/storeCommercialAreas', 'PostsController.storeCommercialAreas')
Route.any('/storeRwas', 'PostsController.storeRwas')
Route.any('/storeOwcs', 'PostsController.storeOwcs')

// Report Driver Performance
Route.any('/reportUpload').render('report_upload')
Route.any('/uploadReportFile', 'ReportController.uploadReportFile')
Route.any('/storeDriverPerformance', 'ReportController.storeDriverPerformance')
Route.any('/importDriverPerformance', 'ReportController.importDriverPerformance')
Route.any('/getDriverPerformanceReport', 'ReportController.getDriverPerformanceReport')

Route.any('/importVehicleDeployment', 'ReportController.importVehicleDeployment')
Route.any('/storeVehicleDeployment', 'ReportController.storeVehicleDeployment')
Route.any('/getVehicleDeploymentReport', 'ReportController.getVehicleDeploymentReport')

Route.any('/importDriverTopPerformance', 'ReportController.importDriverTopPerformance')
Route.any('/storeDriverTopPerformance', 'ReportController.storeDriverTopPerformance')
Route.any('/getDriverTopPerformanceReport', 'ReportController.getDriverTopPerformanceReport')

Route.any('/importDriverWorstPerformance', 'ReportController.importDriverWorstPerformance')
Route.any('/storeDriverWorstPerformance', 'ReportController.storeDriverWorstPerformance')
Route.any('/getDriverWorstPerformanceReport', 'ReportController.getDriverWorstPerformanceReport')
// User Login
Route.any('login', 'UserController.login')
Route.any('register', 'UserController.doRegister')

