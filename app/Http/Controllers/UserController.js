'use strict'
const User   = use('App/Model/User');
const Hash = use('Hash')


class UserController {
	
	* login(request, response) {
	    const email = request.input('email')
	    const password = request.input('password')

	    const loginMessage = {
	        success: 'Logged-in Successfully!',
	        error: 'Invalid Credentials'
	    }

	    // Attempt to login with email and password
	    const authCheck = yield request.auth.attempt(email, password)
	    if (authCheck) {
	    	const user  = yield User.findBy('email', email);
	        yield response.send(user)
	    }
		yield response.send({ error: loginMessage.error })
	}

	* logout(request, response) {
	    yield request.auth.logout()
	    yield response.send({ success : true })
	}

  	* doRegister(request, response) {
        const user = new User()
        user.email = request.input('email')
        user.password = yield Hash.make(request.input('password'))
        user.user_role = 2;
        user.firstname = request.input('firstname');
        user.lastname = request.input('lastname');;
        user.mobile = request.input('mobile');;
        yield user.save()

        var registerMessage = {
            success: 'Registration Successful! Now go ahead and login'
        }

        yield response.send({ registerMessage : registerMessage })
    }
}

module.exports = UserController
