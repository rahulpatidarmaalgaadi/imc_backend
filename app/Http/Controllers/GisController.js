'use strict'

const PetrolPump = use('App/Model/PetrolPump')
const CityParks = use('App/Model/CityParks')
const TwinBin = use('App/Model/TwinBin')
const CtPt = use('App/Model/CtPt')
const Database = use('Database');
const Gvps = use('App/Model/Gvps');
const CommercialAreas = use('App/Model/CommercialAreas');
const Helpers = use('Helpers')
const Owcs = use('App/Model/Owcs');
const Rwas = use('App/Model/Rwas');

class GisController {

	* getPetrolPumpList (request, response) {
     	const data = request.all();
     	
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	 
	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const petrolPump = yield PetrolPump.query().where('zone_number', zoneNumber).where('ward_number', wardNumber)
	 		return yield response.send(petrolPump);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const petrolPump = yield PetrolPump.query().where('zone_number', zoneNumber)
	 		return yield response.send(petrolPump);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const petrolPump = yield PetrolPump.query().where('ward_number', wardNumber)
	 		return yield response.send(petrolPump);
	 	}
	 	else
	 	{
	 		const petrolPump = yield PetrolPump.query()
	 		return yield response.send(petrolPump);
	 	}
	 	return yield response.send([]);
	}

	* getTwinBinsList (request, response) { 
	    const data = request.all();
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const twinBin = yield TwinBin.query().where('zone_number', zoneNumber).where('ward_number', wardNumber)
	 		return yield response.send(twinBin);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const twinBin = yield TwinBin.query().where('zone_number', zoneNumber)
	 		return yield response.send(twinBin);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const twinBin = yield TwinBin.query().where('ward_number', wardNumber)
	 		return yield response.send(twinBin);
	 	}
	 	else
	 	{
	 		const twinBin = yield TwinBin.query()
	 		return yield response.send(twinBin);
	 	}
	 	return yield response.send([]);
	}

	* getCTPTList (request, response) { 
		const data = request.all();
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const ctPt = yield CtPt.query().where('zone_number', zoneNumber).where('ward_number', wardNumber)
	 		return yield response.send(ctPt);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const ctPt = yield CtPt.query().where('zone_number', zoneNumber)
	 		return yield response.send(ctPt);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const ctPt = yield CtPt.query().where('ward_number', wardNumber)
	 		return yield response.send(ctPt);
	 	}
	 	else
	 	{
	 		const ctPt = yield CtPt.query()
	 		return yield response.send(ctPt);
	 	}
	 	return yield response.send([]);
	}

	* getCityParksList (request, response) { 
		const data = request.all();
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const cityParks = yield CityParks.query().where('zone_number', zoneNumber).where('ward_number', wardNumber)
	 		return yield response.send(cityParks);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const cityParks = yield CityParks.query().where('zone_number', zoneNumber)
	 		return yield response.send(cityParks);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const cityParks = yield CityParks.query().where('ward_number', wardNumber)
	 		return yield response.send(cityParks);
	 	}
	 	else
	 	{
	 		const cityParks = yield CityParks.query()
	 		return yield response.send(cityParks);
	 	}
	 	return yield response.send([]);
	    
	}

	* getGvpsList (request, response) { 
		const data = request.all();
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const gvps = yield Gvps.query().where('zone_number', zoneNumber).where('ward_number', wardNumber)
	 		return yield response.send(gvps);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const gvps = yield Gvps.query().where('zone_number', zoneNumber)
	 		return yield response.send(gvps);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const gvps = yield Gvps.query().where('ward_number', wardNumber)
	 		return yield response.send(gvps);
	 	}
	 	else
	 	{
	 		const gvps = yield Gvps.query()
	 		return yield response.send(gvps);
	 	}
	 	return yield response.send([]);
	}
    
    * getCommercialAreasList (request, response) { 
		const data = request.all();
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const commercialAreas = yield CommercialAreas.query().where('zone_number', zoneNumber).where('ward_number', wardNumber)
	 		return yield response.send(commercialAreas);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const commercialAreas = yield CommercialAreas.query().where('zone_number', zoneNumber)
	 		return yield response.send(commercialAreas);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const commercialAreas = yield CommercialAreas.query().where('ward_number', wardNumber)
	 		return yield response.send(commercialAreas);
	 	}
	 	else
	 	{
	 		const commercialAreas = yield CommercialAreas.query()
	 		return yield response.send(commercialAreas);
	 	}
	 	return yield response.send([]);
	}

	* getRwasList (request, response) { 
		const data = request.all();
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const rwas = yield Rwas.query().where('zone_number', zoneNumber).where('ward_number', wardNumber)
	 		return yield response.send(rwas);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const rwas = yield Rwas.query().where('zone_number', zoneNumber)
	 		return yield response.send(rwas);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const rwas = yield Rwas.query().where('ward_number', wardNumber)
	 		return yield response.send(rwas);
	 	}
	 	else
	 	{
	 		const rwas = yield Rwas.query()
	 		return yield response.send(rwas);
	 	}
	 	return yield response.send([]);
	}

	* getOwcsList (request, response) { 
		const data = request.all();
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const owcs = yield Owcs.query().where('zone_number', zoneNumber).where('ward_number', wardNumber)
	 		return yield response.send(owcs);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const owcs = yield Owcs.query().where('zone_number', zoneNumber)
	 		return yield response.send(owcs);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const owcs = yield Owcs.query().where('ward_number', wardNumber)
	 		return yield response.send(owcs);
	 	}
	 	else
	 	{
	 		const owcs = yield Owcs.query()
	 		return yield response.send(owcs);
	 	}
	 	return yield response.send([]);
	}

	

    

}// end Class


module.exports = GisController
