'use strict'

const DriverPerformance = use('App/Model/DriverPerformance')
const DriverBestPerformance = use('App/Model/DriverBestPerformance')
const DriverWorstPerformance = use('App/Model/DriverWorstPerformance')
const VehicleDeployment = use('App/Model/VehicleDeployment')
const Helpers = use('Helpers')


class ReportController {

	* uploadReportFile(request, response){
		const data = request.all();
		
		var fileType = data.file_type == undefined ? '' : data.file_type;
		const fs = require('fs');
		const upFile = request.file('excel', {
			types: ['image'],
			size: '2mb',
		})
		
		if(fileType == 'driver_performance')
		{
			var fileName = 'driver_performance.xls'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeDriverPerformance')
		}

		if(fileType == 'driver_best_performance')
		{
			var fileName = 'driver_best_performance.xls'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeDriverTopPerformance')
		}

		if(fileType == 'driver_worst_performance')
		{
			var fileName = 'driver_worst_performance.xls'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeDriverWorstPerformance')
		}

		if(fileType == 'vehicle_deployment')
		{
			var fileName = 'vehicle_deployment.xls'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeVehicleDeployment')
		}
	}

	* storeDriverPerformance(request, response){
		const fs = require('fs');
		let reqPath = Helpers.publicPath('uploads/driver_performance.xls');
	    let outPath = Helpers.publicPath('uploads/driver_performance.json');
	    const node_xj = require("xls-to-json");
	    
		node_xj({
			input: reqPath, 
			output: outPath 
		}, function(err, result) {
			if(err) {
				console.error(err);
			} else {
				return response.redirect('/importDriverPerformance')
			}
		});
	}
	
	* importDriverPerformance(request, response){
		const fs = require('fs');
	    let reqPath = Helpers.publicPath('uploads/driver_performance.json');
	    let rawdata = fs.readFileSync(reqPath);  
	    var data = JSON.parse(rawdata);  
	    var newData = [];
		for (var i = 0; i < data.length; i++) {
			if(data[i].ward_no != '')
			{
				const dataObj = new DriverPerformance()
				dataObj.vehicle_rto = data[i].vehicle_rto;
				dataObj.ward_no = data[i].ward_no;
				dataObj.zone_no = data[i].zone_no;
				dataObj.driver_name = data[i].driver_name;
				dataObj.vehicle_average = data[i].vehicle_average;
				dataObj.marks_vehicle_average = data[i].marks_vehicle_average;
				dataObj.maintainance_servicing = data[i].maintainance_servicing;
				dataObj.marks_maintainance_servicing = data[i].marks_maintainance_servicing;
				dataObj.attendance = data[i].attendance;
				dataObj.marks_attendance = data[i].marks_attendance;
				dataObj.late_starting_status = data[i].late_starting_status;
				dataObj.marks_late_starting_status = data[i].marks_late_starting_status;
				dataObj.speed_violation = data[i].speed_violation;
				dataObj.marks_speed_violation = data[i].marks_speed_violation;
				dataObj.total_marks = data[i].total_marks;
				dataObj.month = data[i].month;
				dataObj.year = data[i].year;
				yield dataObj.save()
			}
			
		}
		fs.truncate(reqPath, '', function(err){ console.log(err); })
		let reqPathXls = Helpers.publicPath('uploads/driver_performance.xls');
		fs.unlinkSync(reqPathXls);
		//response.send(data);
		response.redirect('/reportUpload')
	}

	* storeDriverTopPerformance(request, response){
		const fs = require('fs');
		let reqPath = Helpers.publicPath('uploads/driver_best_performance.xls');
	    let outPath = Helpers.publicPath('uploads/driver_best_performance.json');
	    const node_xj = require("xls-to-json");
	    
		node_xj({
			input: reqPath, 
			output: outPath 
		}, function(err, result) {
			if(err) {
				console.error(err);
			} else {
				return response.redirect('/importDriverTopPerformance')
			}
		});
	}
	
	* importDriverTopPerformance(request, response){
		const fs = require('fs');
	    let reqPath = Helpers.publicPath('uploads/driver_best_performance.json');
	    let rawdata = fs.readFileSync(reqPath);  
	    var data = JSON.parse(rawdata);  
	    var newData = [];
		for (var i = 0; i < data.length; i++) {
			const dataObj = new DriverBestPerformance()
				dataObj.vehicle_rto = data[i].vehicle_rto;
				dataObj.zone_no = data[i].zone_no;
				dataObj.driver_name = data[i].driver_name;
				dataObj.month = data[i].month;
				dataObj.year = data[i].year;
				dataObj.remark = data[i].remark;

				yield dataObj.save()
			
		}
		fs.truncate(reqPath, '', function(err){ console.log(err); })
		let reqPathXls = Helpers.publicPath('uploads/driver_best_performance.xls');
		fs.unlinkSync(reqPathXls);
		//response.send(data);
		response.redirect('/reportUpload')
	}

	* storeDriverWorstPerformance(request, response){
		const fs = require('fs');
		let reqPath = Helpers.publicPath('uploads/driver_worst_performance.xls');
	    let outPath = Helpers.publicPath('uploads/driver_worst_performance.json');
	    const node_xj = require("xls-to-json");
	    
		node_xj({
			input: reqPath, 
			output: outPath 
		}, function(err, result) {
			if(err) {
				console.error(err);
			} else {
				return response.redirect('/importDriverWorstPerformance')
			}
		});
	}
	
	* importDriverWorstPerformance(request, response){
		const fs = require('fs');
	    let reqPath = Helpers.publicPath('uploads/driver_worst_performance.json');
	    let rawdata = fs.readFileSync(reqPath);  
	    var data = JSON.parse(rawdata);  
	    var newData = [];
		for (var i = 0; i < data.length; i++) {
			if(data[i].ward_no != '')
			{
				const dataObj = new DriverWorstPerformance()
				dataObj.vehicle_rto = data[i].vehicle_rto;
				dataObj.zone_no = data[i].zone_no;
				dataObj.driver_name = data[i].driver_name;
				dataObj.remark = data[i].remark;
				dataObj.month = data[i].month;
				dataObj.year = data[i].year;
				yield dataObj.save()
			}
			
		}
		fs.truncate(reqPath, '', function(err){ console.log(err); })
		let reqPathXls = Helpers.publicPath('uploads/driver_worst_performance.xls');
		fs.unlinkSync(reqPathXls);
		//response.send(data);
		response.redirect('/reportUpload')
	}

	* getDriverPerformanceReport(request, response){
		const data = request.all();
     	
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	var month 	   = data.month == undefined ? '' : data.month;
	 	var year       = data.year == undefined ? '' : data.year;
	 	if(month == '')
	 	{
	 		return yield response.ok('Month Parameter is reuired');
	 	}
	 	if(year == '')
	 	{
	 		return yield response.ok('Year Parameter is reuired');
	 	}

	 	if(zoneNumber != '' && wardNumber != '')
	 	{
	 		const driverPerformance = yield DriverPerformance.query().where('zone_no', zoneNumber).where('ward_no', wardNumber).where('month', month).where('year', year)
	 		return yield response.send(driverPerformance);
	 	}
	 	else if(zoneNumber != '' && wardNumber == '')
	 	{
	 		const driverPerformance = yield DriverPerformance.query().where('zone_no', zoneNumber).where('month', month).where('year', year)
	 		return yield response.send(driverPerformance);
	 	}
	 	else if(zoneNumber == '' && wardNumber != '')
	 	{
	 		const driverPerformance = yield DriverPerformance.query().where('ward_no', wardNumber).where('month', month).where('year', year)
	 		return yield response.send(driverPerformance);
	 	}
	 	else
	 	{ 
	 		const driverPerformance = yield DriverPerformance.query().where('month', month).where('year', year)
	 		return yield response.send(driverPerformance);
	 	}
	 	return yield response.send();
	}

	* getDriverTopPerformanceReport(request, response){
		const data = request.all();
     	
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var month 	   = data.month == undefined ? '' : data.month;
	 	var year       = data.year == undefined ? '' : data.year;
	 	if(month == '')
	 	{
	 		return yield response.ok('Month Parameter is reuired');
	 	}
	 	if(year == '')
	 	{
	 		return yield response.ok('Year Parameter is reuired');
	 	}

	 	if(zoneNumber != '')
	 	{
	 		const driverPerformance = yield DriverBestPerformance.query().where('zone_no', zoneNumber).where('month', month).where('year', year)
	 		return yield response.send(driverPerformance);
	 	}
	 	else
	 	{ 
	 		const driverPerformance = yield DriverBestPerformance.query().where('month', month).where('year', year)
	 		return yield response.send(driverPerformance);
	 	}
	 	return yield response.send();
	}

	* getDriverWorstPerformanceReport(request, response){
		const data = request.all();
     	
     	var zoneNumber = data.zone_number == undefined ? '' : data.zone_number;
	 	var month 	   = data.month == undefined ? '' : data.month;
	 	var year       = data.year == undefined ? '' : data.year;
	 	if(month == '')
	 	{
	 		return yield response.ok('Month Parameter is reuired');
	 	}
	 	if(year == '')
	 	{
	 		return yield response.ok('Year Parameter is reuired');
	 	}

	 	if(zoneNumber != '')
	 	{
	 		const driverPerformance = yield DriverWorstPerformance.query().where('zone_no', zoneNumber).where('month', month).where('year', year)
	 		return yield response.send(driverPerformance);
	 	}
	 	else
	 	{ 
	 		const driverPerformance = yield DriverWorstPerformance.query().where('month', month).where('year', year)
	 		return yield response.send(driverPerformance);
	 	}
	 	return yield response.send();
	}


	* storeVehicleDeployment(request, response){
		const fs = require('fs');
		let reqPath = Helpers.publicPath('uploads/vehicle_deployment.xls');
	    let outPath = Helpers.publicPath('uploads/vehicle_deployment.json');
	    const node_xj = require("xls-to-json");
	    
		node_xj({
			input: reqPath, 
			output: outPath 
		}, function(err, result) {
			if(err) {
				console.error(err);
			} else {
				return response.redirect('/importVehicleDeployment')
			}
		});
	}
	
	* importVehicleDeployment(request, response){
		const fs = require('fs');
	    let reqPath = Helpers.publicPath('uploads/vehicle_deployment.json');
	    let rawdata = fs.readFileSync(reqPath);  
	    var data = JSON.parse(rawdata);  
	    var newData = [];
		for (var i = 0; i < data.length; i++) {
			if(data[i].ward_no != '')
			{
				const dataObj = new VehicleDeployment()
				dataObj.ward_no = data[i].ward_no;
				dataObj.vehicle_no = data[i].vehicle_no;
				dataObj.gps_id = data[i].gps_id;
				dataObj.employee_id = data[i].employee_id;
				dataObj.driver_name = data[i].driver_name;
				dataObj.month = data[i].month;
				dataObj.year = data[i].year;
				yield dataObj.save()
			}
			
		}
		fs.truncate(reqPath, '', function(err){ console.log(err); })
		let reqPathXls = Helpers.publicPath('uploads/vehicle_deployment.xls');
		fs.unlinkSync(reqPathXls);
		//response.send(data);
		response.redirect('/reportUpload')
	}

	* getVehicleDeploymentReport(request, response){
		const data = request.all();
     	
     	var wardNumber = data.ward_number == undefined ? '' : data.ward_number;
	 	var month 	   = data.month == undefined ? '' : data.month;
	 	var year       = data.year == undefined ? '' : data.year;
	 	if(month == '')
	 	{
	 		return yield response.ok('Month Parameter is reuired');
	 	}
	 	if(year == '')
	 	{
	 		return yield response.ok('Year Parameter is reuired');
	 	}

	 	if(wardNumber != '')
	 	{
	 		const vehicleDeployment = yield VehicleDeployment.query().where('ward_no', wardNumber).where('month', month).where('year', year)
	 		return yield response.send(vehicleDeployment);
	 	}
	 	else
	 	{ 
	 		const vehicleDeployment = yield VehicleDeployment.query().where('month', month).where('year', year)
	 		return yield response.send(vehicleDeployment);
	 	}
	 	return yield response.send();
	}
	
}// end Class

module.exports = ReportController
