'use strict'

const PetrolPump = use('App/Model/PetrolPump')
const CityParks = use('App/Model/CityParks')
const TwinBin = use('App/Model/TwinBin')
const CtPt = use('App/Model/CtPt')
const Database = use('Database');
const Gvps = use('App/Model/Gvps');
const CommercialAreas = use('App/Model/CommercialAreas');
const Owcs = use('App/Model/Owcs');
const Rwas = use('App/Model/Rwas');
const Helpers = use('Helpers')


class PostsController {

	* uploadFile(request, response){
		const data = request.all();
		
		var fileType = data.file_type == undefined ? '' : data.file_type;
		const fs = require('fs');
		const upFile = request.file('excel', {
			types: ['image'],
			size: '2mb',
			allowedExtensions: ['geojson']
		})
		
		if(fileType == 'petrol_pump')
		{
			var fileName = 'petrol_pump.geojson'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storePump')
		}
		if(fileType == 'cp_ct')
		{
			var fileName = 'cp_ct.geojson'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeCtpt')
		}
		if(fileType == 'gvps')
		{
			var fileName = 'gvps.geojson'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeGvps')
		}
		if(fileType == 'twin_bins')
		{
			var fileName = 'twin_bins.geojson'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeTwinBin')
		}
		if(fileType == 'city_parks')
		{
			var fileName = 'city_parks.geojson'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeCityParks')
		}
		if(fileType == 'commercial_areas')
		{
			var fileName = 'commercial_areas.geojson'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeCommercialAreas')
		}
		if(fileType == 'rwas')
		{
			var fileName = 'rwas.geojson'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeRwas')
		}
		if(fileType == 'owcs')
		{
			var fileName = 'owcs.geojson'
			yield upFile.move(Helpers.publicPath('uploads'), fileName)
			response.redirect('/storeOwcs')
		}
	}

	* storePump(request, response){
		const fs = require('fs');
		yield Database.truncate('petrol_pump')
		let reqPath = Helpers.publicPath('uploads/petrol_pump.geojson');
	    let rawdata = fs.readFileSync(reqPath);  
	    var dataParse = JSON.parse(rawdata);  
	    var data = dataParse.features
    	var newData = [];
		for (var i = 0; i < data.length; i++) {
			const dataObj = new PetrolPump()
			var geoCord = data[i].geometry.coordinates;
			dataObj.long = geoCord[0]
			dataObj.lat = geoCord[1]
			dataObj.address = data[i].properties.Address;
			dataObj.ward_number = data[i].properties.wardno;
			dataObj.zone_number = data[i].properties.zone_no;
			// dataObj.sunday_hours = data[i].properties.Sunday_hou;
			// dataObj.monday_hours = data[i].properties.Monday_hou;
			// dataObj.tuesday_hours = data[i].properties.Tuesday_ho;
			// dataObj.wednesday_hours = data[i].properties.Wednesday;
			// dataObj.thursday_hours = data[i].properties.Thursday_h;
			// dataObj.friday_hours = data[i].properties.Friday_hou;
			// dataObj.saturday_hours = data[i].properties.Saturday_h;
			dataObj.sunday_hours = '06:00-23:00';
			dataObj.monday_hours = '06:00-23:00';
			dataObj.tuesday_hours = '06:00-23:00';
			dataObj.wednesday_hours = '06:00-23:00';
			dataObj.thursday_hours = '06:00-23:00';
			dataObj.friday_hours = '06:00-23:00';
			dataObj.saturday_hours = '06:00-23:00';
			dataObj.company = data[i].properties.company;

			console.log('data[i].properties', data[i].properties);
			yield dataObj.save()
		}
		response.redirect('/upload')
	}


	* storeCtpt(request, response){
		const fs = require('fs');
		yield Database.truncate('ct_pt')
	    let reqPath = Helpers.publicPath('uploads/cp_ct.geojson');
	    let rawdata = fs.readFileSync(reqPath);  
	    var dataParse = JSON.parse(rawdata);  
	    var data = dataParse.features
	    var newData = [];
	    for (var i = 0; i < data.length; i++) {
			const dataObj = new CtPt()
			var geoCord = data[i].geometry.coordinates;
			dataObj.long = geoCord[0]
			dataObj.lat = geoCord[1]
			dataObj.toilet_id = data[i].properties.TOILET_ID
			dataObj.toilet_it_pick_ct_pt = data[i].properties.TOILET_TYPE
			dataObj.ward_number = data[i].properties.WARD_NO
			dataObj.zone_number = data[i].properties.ZONE_NO
			newData.push(dataObj)
			yield dataObj.save()
		}
		response.redirect('/upload')
	}

	* storeGvps(request, response){
		const fs = require('fs');
		yield Database.truncate('gvps')
	    let reqPath = Helpers.publicPath('uploads/gvps.geojson');
	    let rawdata = fs.readFileSync(reqPath);  
	    var dataParse = JSON.parse(rawdata);  
	    var data = dataParse.features
	    var newData = [];
	    for (var i = 0; i < data.length; i++) {
			const dataObj = new Gvps()
			var geoCord = data[i].geometry.coordinates;
			dataObj.name = data[i].properties.Name
			dataObj.images = data[i].properties.Images
			dataObj.ward_number = data[i].properties.wardno
			dataObj.zone_number = data[i].properties.Zone_No
			dataObj.long = geoCord[0]
			dataObj.lat = geoCord[1]

			newData.push(dataObj)
			yield dataObj.save()
		}

		response.redirect('/upload')
	}

	* storeTwinBin(request, response){
		const fs = require('fs');
		yield Database.truncate('twin_bins')
	    let reqPath = Helpers.publicPath('uploads/twin_bins.geojson');
	    let rawdata = fs.readFileSync(reqPath);  
	    var dataParse = JSON.parse(rawdata);  
	    var data = dataParse.features
	    var newData = [];
	    for (var i = 0; i < data.length; i++) {
			const dataObj = new TwinBin()
			var geoCord = data[i].geometry.coordinates;
			dataObj.long = String(geoCord[0]);
			dataObj.lat = String(geoCord[1]);
			dataObj.address = data[i].properties.Commission || '';
			dataObj.ward_number = parseInt(data[i].properties.wardno) || 0;
			dataObj.zone_number = parseInt(data[i].properties.zone_no) || 0;
			newData.push(dataObj)
			yield dataObj.save()
		}
		response.redirect('/upload')
	}


	* storeCityParks(request, response){
		const fs = require('fs');
		yield Database.truncate('city_parks')
	    let reqPath = Helpers.publicPath('uploads/city_parks.geojson');
	    let rawdata = fs.readFileSync(reqPath);  
	    var dataParse = JSON.parse(rawdata);  
	    var data = dataParse.features
	    var newData = [];
	    for (var i = 0; i < data.length; i++) {
			const dataObj = new CityParks()
			var geoCord = data[i].geometry.coordinates;
			dataObj.long = geoCord[0]
			dataObj.lat = geoCord[1]
			dataObj.name = data[i].properties.Name
			dataObj.ward_number = data[i].properties.wardno
			dataObj.zone_number = data[i].properties.zone_no
			newData.push(dataObj)
			yield dataObj.save()
		}
		response.redirect('/upload')
	}

	* storeCommercialAreas(request, response){
		const fs = require('fs');
		yield Database.truncate('commercial_areas')
	    let reqPath = Helpers.publicPath('uploads/commercial_areas.geojson');
	    let rawdata = fs.readFileSync(reqPath);  
	    var dataParse = JSON.parse(rawdata);  
	    var data = dataParse.features
	    var newData = [];
	    for (var i = 0; i < data.length; i++) {
			const dataObj = new CommercialAreas()
			var geoCord = data[i].geometry.coordinates[0];
			var geoCord1 = geoCord[0]
			var geoCodeObj = {}
			var newGeoArr = [];
			for (var k = 0; k < geoCord1.length; k++) {
				var long  = geoCord1[k][0]
				var lat = geoCord1[k][1]
				geoCodeObj = {lat,long}
				newGeoArr.push(geoCodeObj);
			}
			dataObj.name = data[i].properties.Name
			dataObj.coordinates = JSON.stringify(newGeoArr)
			dataObj.ward_number = data[i].properties.wardno
			dataObj.zone_number = data[i].properties.zone_no
			newData.push(dataObj)
			yield dataObj.save()
		}
		return response.redirect('/upload')
	}

	* storeRwas(request, response){
		const fs = require('fs');
		yield Database.truncate('rwas')
	    let reqPath = Helpers.publicPath('uploads/rwas.geojson');
	    let rawdata = fs.readFileSync(reqPath);  
	    var dataParse = JSON.parse(rawdata);  
	    var data = dataParse.features
	    var newData = [];
	    for (var i = 0; i < data.length; i++) {
			const dataObj = new Rwas()
			var geoCord = data[i].geometry.coordinates[0];
			dataObj.long = geoCord[0]
			dataObj.lat = geoCord[1]
			dataObj.name = data[i].properties.Name
			dataObj.ward_number = data[i].properties.wardno
			dataObj.zone_number = data[i].properties.zone_no
			newData.push(dataObj)
			yield dataObj.save()
		}
		response.redirect('/upload')
	}

	* storeOwcs(request, response){
		const fs = require('fs');
		yield Database.truncate('owcs')
	    let reqPath = Helpers.publicPath('uploads/owcs.geojson');
	    let rawdata = fs.readFileSync(reqPath);  
	    var dataParse = JSON.parse(rawdata);  
	    var data = dataParse.features
	    var newData = [];
	    for (var i = 0; i < data.length; i++) {
			const dataObj = new Owcs()
			var geoCord = data[i].geometry.coordinates;
			dataObj.long = geoCord[0]
			dataObj.lat = geoCord[1]
			dataObj.name = data[i].properties.Name
			dataObj.ward_number = data[i].properties.wardno
			dataObj.zone_number = data[i].properties.zone_no
			newData.push(dataObj)
			yield dataObj.save()
		}
		response.redirect('/upload')
	}




}// end Class

module.exports = PostsController
