'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('firstname')
      table.string('lastname')
      table.string('mobile').unique()
      table.string('email').unique()
      table.string('password',60)
      table.integer('user_role')
      table.timestamps()
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
