'use strict'

const Schema = use('Schema')

class OwcsTableSchema extends Schema {
  up () {
    this.create('owcs', (table) => {
      table.increments('id')
      table.string('lat')
      table.string('long')
      table.string('name')
      table.integer('ward_number')
      table.integer('zone_number')
      table.timestamps()
    })
  }

  down () {
    this.drop('owcs')
  }
}

module.exports = OwcsTableSchema
