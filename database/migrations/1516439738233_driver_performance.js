'use strict'

const Schema = use('Schema')

class DriverPerformaceTableSchema extends Schema {

  up () {
    this.table('driver_performance', (table) => {
      // alter driver_performace table
      table.string('month').after('total_marks')
      table.string('year')
      
    })
  }

  down () {
    this.table('driver_performance', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = DriverPerformaceTableSchema
