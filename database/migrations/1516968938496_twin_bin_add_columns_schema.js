'use strict'

const Schema = use('Schema')

class TwinBinAddColumnsSchema extends Schema {
  up () {
    this.table('twin_bins', (table) => {
      table.integer('ward_number').nullable().after('address');
      table.integer('zone_number').nullable().after('ward_number');
    })
  }

  down () {
    this.table('twin_bin_add_columns', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TwinBinAddColumnsSchema
