'use strict'

const Schema = use('Schema')

class DriverPerformanceSchema extends Schema {
  up () {
    this.create('driver_performance', (table) => {
      table.increments('id')
      table.string('vehicle_rto')
      table.integer('ward_no')
      table.integer('zone_no')
      table.integer('month')
      table.integer('year')
      table.string('driver_name')
      table.string('vehicle_average')
      table.string('marks_vehicle_average')
      table.string('maintainance_servicing')
      table.string('marks_maintainance_servicing')
      table.string('attendance')
      table.string('marks_attendance')
      table.string('late_starting_status')
      table.string('marks_late_starting_status')
      table.string('speed_violation')
      table.string('marks_speed_violation')
      table.string('total_marks')
      table.timestamps()
    })
  }

  down () {
    this.drop('driver_performance')
  }
}

module.exports = DriverPerformanceSchema
