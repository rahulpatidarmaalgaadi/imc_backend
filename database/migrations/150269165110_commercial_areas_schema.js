'use strict'

const Schema = use('Schema')

class CommercialAreasSchema extends Schema {
  up () {
    this.create('commercial_areas', (table) => {
      table.increments('id')
      table.string('name')
      table.integer('ward_number')
      table.integer('zone_number')
      table.longText('coordinates')
      table.timestamps()
    })
  }

  down () {
    this.drop('commercial_areas')
  }
}

module.exports = CommercialAreasSchema
