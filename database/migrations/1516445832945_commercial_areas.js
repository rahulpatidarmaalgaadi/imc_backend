'use strict'

const Schema = use('Schema')

class CommercialAreasTableSchema extends Schema {

  up () {
    this.table('commercial_areas', (table) => {
      table.text('coordinates', 'longtext').after('name');
    })
  }

  down () {
    this.table('commercial_areas', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = CommercialAreasTableSchema
