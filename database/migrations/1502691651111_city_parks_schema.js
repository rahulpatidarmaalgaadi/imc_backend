'use strict'

const Schema = use('Schema')

class CityParksSchema extends Schema {
  up () {
    this.create('city_parks', (table) => {
      table.increments('id')
      table.string('lat')
      table.string('long')
      table.string('name')
      table.integer('ward_number')
      table.integer('zone_number')
      table.timestamps()
    })
  }

  down () {
    this.drop('city_parks')
  }
}

module.exports = CityParksSchema
