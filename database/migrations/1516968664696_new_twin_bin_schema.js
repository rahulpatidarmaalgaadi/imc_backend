'use strict'

const Schema = use('Schema')

class NewTwinBinSchema extends Schema {
  up () {
    this.table('twin_bins', (table) => {
      table.dropColumn('ward_number');
      table.dropColumn('zone_number');
    })
  }

  down () {
    this.table('new_twin_bins', (table) => {
      // reverse alternations
    })
  }
}

module.exports = NewTwinBinSchema
