'use strict'

const Schema = use('Schema')

class TwinBinsSchema extends Schema {
  up () {
    this.create('twin_bins', (table) => {
      table.increments('id')
      table.string('lat')
      table.string('long')
      table.string('address')
      table.integer('ward_number')
      table.integer('zone_number')
      table.timestamps()
    })
  }

  down () {
    this.drop('twin_bins')
  }
}

module.exports = TwinBinsSchema
