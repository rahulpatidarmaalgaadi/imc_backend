'use strict'

const Schema = use('Schema')

class PetrolPumpSchema extends Schema {
  up () {
    this.create('petrol_pump', (table) => {
      table.increments('id')
      table.string('lat')
      table.string('long')
      table.string('address')
      table.integer('ward_number')
      table.integer('zone_number')
      table.string('sunday_hours')
      table.string('monday_hours')
      table.string('tuesday_hours')
      table.string('wednesday_hours')
      table.string('thursday_hours')
      table.string('friday_hours')
      table.string('saturday_hours')
      table.timestamps()
    })
  }

  down () {
    this.drop('petrol_pump')
  }
}

module.exports = PetrolPumpSchema