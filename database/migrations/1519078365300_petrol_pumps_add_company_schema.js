'use strict'

const Schema = use('Schema')

class PetrolPumpsAddCompanySchema extends Schema {
  up () {
    this.table('petrol_pump', (table) => {
      table.string('company').nullable().after('address');

      // alter table
    })
  }

  down () {
    this.table('petrol_pump', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PetrolPumpsAddCompanySchema
