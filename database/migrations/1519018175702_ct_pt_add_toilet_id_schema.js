'use strict'

const Schema = use('Schema')

class CtPtAddToiletIdSchema extends Schema {
  up () {
    this.table('ct_pt', (table) => {
      table.string('toilet_id').nullable().after('toilet_it_pick_ct_pt');
    })
  }

  down () {
    this.table('ct_pt', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CtPtAddToiletIdSchema
