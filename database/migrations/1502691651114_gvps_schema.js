'use strict'

const Schema = use('Schema')

class GvpsSchema extends Schema {
  up () {
    this.create('gvps', (table) => {
      table.increments('id')
      table.string('lat')
      table.string('long')
      table.string('name')
      table.string('images')
      table.integer('ward_number')
      table.integer('zone_number')
      table.timestamps()
    })
  }

  down () {
    this.drop('gvps')
  }
}

module.exports = GvpsSchema
