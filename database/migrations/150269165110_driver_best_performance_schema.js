'use strict'

const Schema = use('Schema')

class DriverBestPerformanceSchema extends Schema {
  up () {
    this.create('driver_best_performance', (table) => {
      table.increments('id')
      table.string('driver_name')
      table.string('vehicle_rto')
      table.integer('zone_no')
      table.integer('month')
      table.integer('year')
      table.string('remark')
      table.timestamps()
    })
  }

  down () {
    this.drop('driver_best_performance')
  }
}

module.exports = DriverBestPerformanceSchema
