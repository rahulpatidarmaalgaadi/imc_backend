'use strict'

const Schema = use('Schema')

class DriverWorstPerformanceSchema extends Schema {
  up () {
    this.create('driver_worst_performance', (table) => {
      table.increments('id')
      table.string('driver_name')
      table.string('vehicle_rto')
      table.integer('zone_no')
      table.integer('month')
      table.integer('year')
      table.string('remark')
      table.timestamps()
    })
  }

  down () {
    this.drop('driver_worst_performance')
  }
}

module.exports = DriverWorstPerformanceSchema
