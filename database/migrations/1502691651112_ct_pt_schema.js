'use strict'

const Schema = use('Schema')

class CtPtSchema extends Schema {
  up () {
    this.create('ct_pt', (table) => {
      table.increments('id')
      table.string('lat')
      table.string('long')
      table.string('toilet_it_pick_ct_pt')
      table.integer('ward_number')
      table.integer('zone_number')
      table.timestamps()
    })
  }

  down () {
    this.drop('ct_pt')
  }
}

module.exports = CtPtSchema
