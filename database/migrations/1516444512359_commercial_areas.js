'use strict'

const Schema = use('Schema')

class CommercialAreasTableSchema extends Schema {

  up () {
    this.table('commercial_areas', (table) => {
      table.dropColumn('coordinates')
    })
  }

  down () {
    this.table('commercial_areas', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = CommercialAreasTableSchema
