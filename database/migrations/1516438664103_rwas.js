'use strict'

const Schema = use('Schema')

class RwasTableSchema extends Schema {
  up () {
    this.create('rwas', (table) => {
      table.increments('id')
      table.string('lat')
      table.string('long')
      table.string('name')
      table.integer('ward_number')
      table.integer('zone_number')
      table.timestamps()
    })
  }

  down () {
    this.drop('rwas')
  }
}

module.exports = RwasTableSchema
