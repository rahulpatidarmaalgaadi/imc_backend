'use strict'

const Schema = use('Schema')

class VehicleDeploymentSchema extends Schema {
  up () {
    this.create('vehicle_deployment', (table) => {
      table.increments('id')
      table.string('ward_no')
      table.string('vehicle_no')
      table.string('gps_id')
      table.string('employee_id')
      table.string('driver_name')
      table.integer('month')
      table.integer('year')
      table.timestamps()
    })
  }

  down () {
    this.drop('vehicle_deployment')
  }
}

module.exports = VehicleDeploymentSchema
